#include <stdbool.h>
#include <stdint.h>
#include <avr/eeprom.h>

#include "uart.h"
#include "toms_uart_extras.h"
#include "defines.h"
#include "input_decoder.h"

//#define UART_DEBUG
#define PRINT_CALIBRATION
/*
  Process the raw input values received from the remote control and turn them
  into more useful data types for further processing.

  We have to do the following things to achieve this:
  * Scale the 16 bit uints into 8 bit ints.
  * Make sure that by wiggling the sticks, we can travel the full range from 
  ..-128 to +127. This is made difficult by two things:
  ..a) There is a large offset the raw minimum value for stick inputs is roughly
  ..   17000.
  ..b) the analog sticks are analog. Their range varies from stick to stick and
  ..   changes over time. This means we will have to repeatedly perform
  ..   calibration.
  * Implement a deadzone for the rough midpoints, so that the robot won't creep
  ..when the sticks are left alone.
*/

//------------------------------------------------------------------------------
//                        Define EEPROM variables here!

/*
  Preseed the eeprom with some maximums and minimums that can be loaded up even
  if an initial calibration has never been done. Based off my own remote control,
  I have chosen numbers that should
  1) Give absolutely horrendous control, albeit still functional. 
  2) And because of that, hopefully serve as a reminder that calibration should
  .. needs to be done.

  These numbers are raw maximum: 24k
  .                 raw minimum: 23k
*/
uint16_t EEMEM raw_maximums[num_channels];
uint16_t EEMEM raw_minimums[num_channels];

void input_decoder_setup(void){
}

static void copy_calibration_data_from_eeprom(inp_decoder_data_t *data) {
  eeprom_read_block((void*) &(data->raw_maximums),
   		    (const void*) raw_maximums,
   		    sizeof(uint16_t) * num_channels);
  eeprom_read_block((void*) &(data->raw_minimums),
   		    (const void*) raw_minimums,
   		    sizeof(uint16_t) * num_channels);
}

#ifdef UART_DEBUG
static void print_raw_maximums(inp_decoder_data_t *data){
  for (uint8_t i = 0; i < num_channels; i++) {
    uart_putln_uint32(data->raw_maximums[i]);
  }
  for (uint8_t i = 0; i < num_channels; i++) {
    uart_putln_uint32(data->raw_minimums[i]);
  }
  uart_putln("");
}
#endif

#ifdef UART_DEBUG
static void print_scaling_factors(inp_decoder_data_t *data){
  uart_putln("\nscaling_factors:");
  for (uint8_t i = 0; i < num_channels; i++) {
    uart_putln_int(data->scaling_factors[i]);
  }
}
#endif

static void load_default_calibration_data_if_copy_failed(inp_decoder_data_t *data) {
  //the eeprom is initialized with all 1s. If there is no data, then load the
  //default data.
  bool only_found_default_data_in_raw_limits = true;
  for (uint8_t i = 0; i < num_channels; i++) {
    if (data->raw_maximums[i] != 0xFFFF) only_found_default_data_in_raw_limits = false;
    if (data->raw_minimums[i] != 0xFFFF) only_found_default_data_in_raw_limits = false;
  }
  if (only_found_default_data_in_raw_limits) {
    for (uint8_t i = 0; i < num_channels; i++) {
      data->raw_maximums[i] = 24000;
      data->raw_minimums[i] = 23000;
    }
  }
}

static void calculate_scaling_factors(inp_decoder_data_t *data){
  for(uint8_t current_ch = 0; current_ch < num_channels; current_ch++) {
    uint16_t range = data->raw_maximums[current_ch] - data->raw_minimums[current_ch];
    data->scaling_factors[current_ch] = range / 256;
  }
#ifdef UART_DEBUG
  print_scaling_factors(data);
#endif
}

static void load_calibration_data(inp_decoder_data_t *data) {
  copy_calibration_data_from_eeprom(data);
  load_default_calibration_data_if_copy_failed(data);
#ifdef UART_DEBUG
  uart_putln("loaded from eeprom:");
  print_raw_maximums(data);
#endif
  calculate_scaling_factors(data);
}

static bool calibration_requested(remote_rx_raw_output_t *raw_output) {
  bool ret = true;
  for (uint8_t i = 0; i < num_channels; i++) {
    if (raw_output->channels[i] < 29000) ret = false;
  }
  return ret;
}

static bool calibration_unrequested(remote_rx_raw_output_t *raw_output){
  bool ret = true;
  for (uint8_t i = 0; i < num_channels; i++) {
    if (raw_output->channels[i] > 19000) ret = false;
  }
  return ret;
}

static void blank_all_values_before_calibration(inp_decoder_data_t *data) {
  for (uint8_t i = 0; i < num_channels; i++) {
    data->raw_maximums[i] = 0;
    data->raw_minimums[i] = UINT16_MAX;
  }
}

static void calibrate(inp_decoder_data_t *data,
		      remote_rx_raw_output_t *raw_output) {
  for (uint8_t i = 0; i < num_channels; i++) {
    if (raw_output->channels[i] > data->raw_maximums[i]) {
      data->raw_maximums[i] = raw_output->channels[i];
    }
    if (raw_output->channels[i] < data->raw_minimums[i]) {
      data->raw_minimums[i] = raw_output->channels[i];
    }
  }
}

static void write_maximums_to_eeprom(inp_decoder_data_t *data){
  eeprom_update_block((const void*) &(data->raw_maximums),
		      (void*)raw_maximums,
		      sizeof(uint16_t) * num_channels);

  eeprom_update_block((const void*) &(data->raw_minimums),
		      (void*)raw_minimums,
		      sizeof(uint16_t) * num_channels);
}

#ifdef UART_DEBUG
static void print_scaled_outputs(inp_decoder_output_t * decoded_output){
  static uint8_t slowdown_timer = 0;
  slowdown_timer++;
  if (slowdown_timer == 6) slowdown_timer = 0;
  if (slowdown_timer == 0){
    for (uint8_t i = 0; i < num_channels; i++) {
      uart_putln_int(decoded_output->channels[i]);
    }
    uart_putln("");
  }
}
#endif

static void scale_one_input(inp_decoder_data_t *data,
			    remote_rx_raw_output_t *raw_output,
			    inp_decoder_output_t * decoded_output,
			    uint8_t i) {
  //Over/underflow protection
  if (raw_output->channels[i] >= data->raw_maximums[i] - 100) {
    decoded_output->channels[i] = UINT8_MAX;
  } else if (raw_output->channels[i] <= data->raw_minimums[i] + 100) {
    decoded_output->channels[i] = 0;
  } else {
    decoded_output->channels[i] = (raw_output->channels[i] - data->raw_minimums[i])
      / data->scaling_factors[i];
  }
}


//Channel 5 is a discrete input with three possible states.
//We will scale this to the numbers printed on the remote control.
static void scale_channel_5(remote_rx_raw_output_t *raw_output,
			    inp_decoder_output_t *decoded_output){
  if (raw_output->channels[4] < 20000) decoded_output->channels[4] = 2;
  else if (raw_output->channels[4] < 27000)  decoded_output->channels[4] = 1;
  else decoded_output->channels[4] = 0;
}

//channel 6 is a discrete input with two states.
static void scale_channel_6(remote_rx_raw_output_t *raw_output,
			    inp_decoder_output_t *decoded_output){
  if (raw_output->channels[5] > 25000) decoded_output->channels[5] = 1;
  else decoded_output->channels[5] = 0;
}

static void scale_all_inputs(inp_decoder_data_t *data,
			     remote_rx_raw_output_t *raw_output,
			     inp_decoder_output_t * decoded_output){
  //scale the analog inputs
  for (uint8_t i = 0; i < 4; i++) {
    scale_one_input(data, raw_output, decoded_output, i);
  }
  
  //scale the discrete inputs
  scale_channel_5(raw_output, decoded_output);
  scale_channel_6(raw_output, decoded_output);
  
#ifdef UART_DEBUG
  print_scaled_outputs(decoded_output);
#endif
}

static void fsm(inp_decoder_data_t *data,
		remote_rx_raw_output_t *raw_output,
		inp_decoder_output_t * decoded_output){
  switch (data->mode) {
  case normal:
    if (calibration_requested(raw_output)) {
#if defined(UART_DEBUG) || defined(PRINT_CALIBRATION)
      uart_putln("entering calibration");
#endif
      blank_all_values_before_calibration(data);
      data->mode = calibration;
      break;
    }
    scale_all_inputs(data, raw_output, decoded_output);
    decoded_output->new_data = true;
    break;
  case calibration:
    if (calibration_unrequested(raw_output)) {
#if defined(UART_DEBUG) || defined(PRINT_CALIBRATION)
      uart_putln("exiting calibration");
#ifdef UART_DEBUG
      print_raw_maximums(data);
#endif
#endif
      write_maximums_to_eeprom(data);
      data->mode = normal;
      calculate_scaling_factors(data);
      break;
    }
    calibrate(data, raw_output);
    break;
  }
}

void input_decoder_loop(inp_decoder_data_t *data,
			remote_rx_raw_output_t *raw_output,
			inp_decoder_output_t * decoded_output){
  if (data->first_pass_finished == false) {
    load_calibration_data(data);
    data->first_pass_finished = true;
  }

  if (raw_output->new_data_received) fsm(data, raw_output, decoded_output);
  else decoded_output->new_data = false;
}
