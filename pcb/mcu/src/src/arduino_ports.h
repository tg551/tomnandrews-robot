#ifndef ARDUINO_PORTS_H
#define ARDUINO_PORTS_H

static const int REMOTE_RX_PIN = DDB4;

//left motor
static const int EN12_PIN = DDD5;
static const int ONEA_PIN = DDD4;
static const int TWOA_PIN = DDB0;

//right motor
static const int EN34_PIN = DDD6;
static const int THREEA_PIN = DDD7;
static const int FOURA_PIN = DDD3;

#endif
