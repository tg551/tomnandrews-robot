// This file exists to pull all timer1's constant and register derivation into a
// single file.

#include <avr/io.h>

#include "timer1_setup.h"

/*
  This timer is to provide timing for comms RX from remote control.
  
  Derivation its prescaler is as follows:
  * The remote control emits a pulse train. we want to be able to get a smooth
  ..range of values as the sticks are moved through their range. Let's say that
  ..we want a range of 256 between stick fully down and stick fully up, but it
  ..doesn't matter if we're worse than that. The selection is arbitrary.
  * The minimum pulse length is 1ms, and the maximum is 2ms.
  * the closer the wraparound point is to 2ms, the more precise the timer will
  ..be. Therefore we want to choose the smallest possible prescale.
  * The MCU clock is 16mhz, and the timer has a count of 2*16
  ..bits. The  and we have the following choice of prescalers: 1, 8,64, 256,
  ..1024.
  * The following expression gives the wraparound time of the
  ..timer: clock period * prescaler * max-timer-value
  * Out of these, the best usable prescaler is 8. It
  ..will wraparound once every (1 / 16_000_000) seconds * 8 *
  .. 2^16.
  ..Which is once every 32.768ms //4.096 ms.
  We set this prescale clearing bits CS12 and CS11 and setting bit CS10 of TCCR1B
    
  We can use the input capture unit to read the timer on a rising edge of the 
  pulse train. This avoids us having to execute an interrupt. to do this, we 
  clear bit ACIC of ACSR (to disable triggering input capture from an analogue
  comparator). We also set the ICES1 bit of TCCR1B to enable it only to capture
  on the rising edge.

  We also set the noise canceller, just for fun!
  

*/
void timer1_setup(void) {
  //Set normal counting mode
  TCCR1B &= ~_BV(WGM13);
  TCCR1B &= ~_BV(WGM12);
  TCCR1A &= ~_BV(WGM11);
  TCCR1A &= ~_BV(WGM10);

  // enable 1/8 prescaler
  //TCCR1B &= ~_BV(CS12);
  //TCCR1B |=  _BV(CS11);
  //TCCR1B &= ~_BV(CS10);

  //Enable the 1/1 prescaler
  TCCR1B &= ~_BV(CS12);
  TCCR1B &= ~_BV(CS11);
  TCCR1B |=  _BV(CS10);
  

  //enable the input capture unit.
    ACSR &= ~_BV(ACIC);

  //Set rising edge detection on the input capture unit.
    TCCR1B |= _BV(ICES1);

  //enable the noise canceller
    TCCR1B |= _BV(ICNC1);
}
