#ifndef DEFINES_H
#define DEFINES_H

#include <stdint.h>

#define UART_BAUD  9600

//remote control defines
static const uint8_t minimum_overflows_for_framing_pulse = 2;
static const uint8_t num_channels = 6;

//Give the channels some meaning
static const uint8_t speed_ch = 1;
static const uint8_t rotation_ch = 3;

#endif
