#include <stdbool.h>
#include <avr/io.h>

#include "remote_rx.h"
#include "uart.h"
#include "toms_uart_extras.h"

#include "defines.h"

//#define UART_DEBUG_REMOTE_RX
//#define UART_DEBUG_RAW_OUTPUT_ONLY

/*
  Decode data sent from the remote control.

  This has been developed around the following radio gear:
  * Spektrum DX5e transmitter. This is advertised as having 5 channels but it
  actually transmits 6.
  * OrangeRX R615x receiver.

  The receiver has a convenient single-wire output form called CPPM (which is a
  pulse train of pulse position modulation signals). This is what we are going to
  decode.

  This PPM sum scheme sends one framing pulse. Its length varies between between
  ~4ms and  ~30 ms). After the framing pulse, it sends 6 further pulses, each
  roughly between 1 and 2 ms.

  To do the decoding, we must measure the gaps between rising edges on the input
  signal, and we will do this using timer1 set to wrap around once per 4.096 ms.

  This scheme will rely on 2 things:
  * The framing pulse will always be longer than two wraparounds of the timer
  (8.192 ms), and a data pulse will always be shorter than 2 wraparounds.
  This allows us to:
  ..a) Detect a framing pulse by observing that timer1 has wrapped around at
  ..   least twice since the last pulse
  ..b) Differentiate a framing pulse from a data pulse by counting wraparounds:
  * The main loop executes faster than the shortest pulse time from the remote
  receiver. If not, then pulses will be missed.

  We will make use of timer1's input capture unit to capture rising edges. This
  is a special piece of hardware which asynchronously stores the exact value of 
  counter1 when  the input on pin B0 changes from low to high. This avoids us 
  having to execute an interrupt whenever the we receive a pulse.
*/



void remote_rx_setup(void) {
  //set pin PB0 to input with pull up resistor engaged
  DDRB &= ~_BV(DDB0);
  PORTB |= _BV(PORTB0);
}

static void clear_input_capture_flag(void) {
  //yes this is counterintuitive. We write a 1 to clear the flag.
  TIFR1 |= _BV(ICF1);
}

static bool is_input_capture_flag_set(void) {
  return TIFR1 & _BV(ICF1);
}

static bool is_overflow_flag_set(void) {
  return TIFR1 & _BV(TOV1);
}

static void clear_overflow_flag(void){
  TIFR1 |= _BV(TOV1);
}

static uint16_t calc_pulse_length(remote_rx_internal_t * data,
				  uint16_t timer1_value) {
  return timer1_value - data->last_pulse;
}

static bool is_pulse_a_framing_pulse(remote_rx_internal_t * data) {
  return data->overflows >= minimum_overflows_for_framing_pulse;
}

#if defined(UART_DEBUG_REMOTE_RX) || defined(UART_DEBUG_RAW_OUTPUT_ONLY)
static void pretty_print_data(remote_rx_raw_output_t * raw_output) {
  uart_putln("");
  for (int i = 0; i < num_channels; i++) {
    uart_putln_int(raw_output->channels[i]);
  }
}
#endif

//Tom's special note: There's actually no FSM here!
static void fsm(remote_rx_internal_t *data,
		remote_rx_raw_output_t *raw_output){
  //before we execute the main logic, we have to do some housekeeping: Read the
  //input capture timer value, calculate the pulse length, and update our
  //internal data.
  uint16_t timer1_value = ICR1;
  //note: the following value is garbage if it's actually a framing pulse.
  uint16_t pulse_length = calc_pulse_length(data, timer1_value);
  data->last_pulse = timer1_value;

  //first we work out what sort of pulse we have. There are two types: data and
  //framing. If we have a framing pulse, then we reset all our internal data and
  //get ready to receive more data pulses. If we have a data pulse, then we 
  //update our data accordingly.
  if (is_pulse_a_framing_pulse(data)) {
#ifdef UART_DEBUG_REMOTE_RX
    uart_putln("f");
#endif
    data->next_channel = 0;
  } else {
    raw_output->channels[data->next_channel] = pulse_length;
#ifdef UART_DEBUG_REMOTE_RX
    uart_puts("d");
    uart_putln_int(data->next_channel);
#endif

    if (data->next_channel < num_channels - 1) {
    data->next_channel++;
  } else {
    raw_output->new_data_received = true;
  }
  }
    data->overflows = 0;
  }

    void remote_rx_loop(remote_rx_internal_t *data,
      remote_rx_raw_output_t *raw_output){
    //First check if we've seen an overflow. We want to count overflows because
    //it's how we differentiate between normal signals and framing signals.

    //Check if we've seen a rising in the input capture unit. If so, print
    //something.
    if (is_overflow_flag_set()) {
    //Record overflows.
    //The overflow counter should never go much higher than 4 or 5, but just in
    //case the receiver breaks or becomes disconnected, we should prevent it from
    //wrapping around to zero
#ifdef UART_DEBUG_REMOTE_RX
    uart_putln("o");
#endif
    if (data->overflows < 255) data->overflows++;
    clear_overflow_flag();
  }
      
    //unconditionally clear the new data flag -- it should only be valid for one 
    //cycle of the main loop.
    raw_output->new_data_received = false;
    
    if (is_input_capture_flag_set()){
    clear_input_capture_flag();
    fsm(data, raw_output);
  }

    //print the received raw data.
    //Only print a proportion of the data frames, because it's impossible to read
    //if they're all printed.
#if defined(UART_DEBUG_REMOTE_RX) || defined(UART_DEBUG_RAW_OUTPUT_ONLY)
    static uint8_t slowdown_counter;;
    if (raw_output->new_data_received) {
      slowdown_counter++;
      if (slowdown_counter == 6) slowdown_counter = 0;
      if (slowdown_counter == 0) pretty_print_data(raw_output);
    }
#endif
  }
