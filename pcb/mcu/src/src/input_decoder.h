#ifndef INPUT_DECODER_H
#define INPUT_DECODER_H

#include <stdbool.h>
#include "defines.h"
#include "remote_rx.h"

typedef enum  {normal, calibration} inp_decoder_state;
typedef struct inp_decoder_data_t {
  bool first_pass_finished;

  uint16_t raw_maximums[num_channels];
  uint16_t raw_minimums[num_channels];
  uint8_t scaling_factors[num_channels];

  inp_decoder_state mode = normal;
} inp_decoder_data_t;


typedef struct inp_decoder_output_t {
  uint8_t channels[num_channels];
  bool new_data;
} inp_decoder_output_t;

void input_decoder_setup(void);
void input_decoder_loop(inp_decoder_data_t*,
			remote_rx_raw_output_t*,
			inp_decoder_output_t*);

#endif
