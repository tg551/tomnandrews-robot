#ifndef MOVEMENT_CONTROLLER_H
#define MOVEMENT_CONTROLLER_H

#include "defines.h"

#include "input_decoder.h"

typedef enum direction_t {forwards, backwards} direction_t;
typedef struct motor_data_t {
  int8_t speed;
} motor_data_t;

typedef struct movement_output_t {
  motor_data_t left_motor, right_motor;
} movement_output_t;

typedef struct movement_data_t {

  motor_data_t left_motor;
  motor_data_t right_motor;
} movement_data_t;

void movement_controller_setup(void);
void movement_controller_loop(movement_data_t *data, 
			      inp_decoder_output_t *inp_decoder_output,
			      movement_output_t *movement_output);

#endif
