#include <stdlib.h>
#include <stdbool.h>
#include <avr/io.h>

#include "defines.h"

#define UART_DEBUG
#ifdef UART_DEBUG
#include "uart.h"
#include "toms_uart_extras.h"
#endif

#include "movement_controller.h"


/*
  let's derive some constants!

  The controlscheme is as follows: The robot has two wheels, one on each side. They are
  capable of going forwards and bacwards. They cannot angle left and right, so
  the robot must use skid steering. Skid steering couples steering and forwards/backwards,
  so we have to invent some scheme to convert independent steering/driving inputs into a
  combined wheel-control output.

  My intention is for the steering input to define a rate of rotation that the robot must
  follow. Given knowledge about the distance between the two wheels, we can work out a
  speeddifference that must be applied to the two wheels, and then add half of it to one
  wheel and subtract half of it to the other wheel.

  If one wheel is going too fast to add half of this speeddifference, then we add as much
  as possible and subtract the rest from the other wheel.

  We may want to set up some sort of washout function for the maximum rate of rotation
  such that the faster the robot is moving, the lower the maximum rate of rotation should
  be. This will prevent the robot from becoming too twitchy
*/

static const float rotation_divider = 1.5; //keep this a multiple of two to make sure
//the compiler implements this as a shift.

static const int motor_timer_prescale = 256;
static const long one_second_time_us = 1000000;
static const long motor_cpu_freq = 16000000;
static const long motor_prescale_tick_time_us = (one_second_time_us /  (motor_cpu_freq / motor_timer_prescale));


static void calc_motor_outputs(movement_data_t *data,
				 inp_decoder_output_t *inp_decoder_output,
				 movement_output_t *movement_output){
  int8_t speed = inp_decoder_output->channels[speed_ch] - (UINT8_MAX / 2) - 1;
  int8_t rotation = inp_decoder_output->channels[rotation_ch] - (UINT8_MAX / 2) - 1;

  int8_t addition_right = rotation / rotation_divider;
  int8_t addition_left = -addition_right;
  
  bool wrap_left_fw, wrap_left_bw, wrap_right_fw, wrap_right_bw;
  wrap_left_fw = wrap_left_bw = wrap_right_fw = wrap_right_bw = false;
  
  if (addition_left > 0) {
    wrap_left_fw = (int8_t)(speed + addition_left) < speed;
  } else {
    wrap_left_bw = (int8_t)(speed + addition_left) > speed;
  }

  if (addition_right > 0){
    wrap_right_fw = (int8_t)(speed + addition_right) < speed;
  } else {
    wrap_right_bw = (int8_t)(speed + addition_right) > speed;
  }

  //rt_putln_int(speed);
  //rt_putln_int(addition_right);
  //art_putln_int((int8_t)(speed + addition_right));

  if (wrap_left_fw) {
    int8_t new_addition_left = INT8_MAX - speed;
    addition_right = addition_right - (addition_left - new_addition_left);
    addition_left = new_addition_left;
  }
  if (wrap_right_fw) {
    int8_t new_addition_right = INT8_MAX - speed;
    addition_left = addition_left - (addition_right - new_addition_right);
    addition_right = new_addition_right;
  }
  if (wrap_left_bw) {
    int8_t new_addition_left = INT8_MIN + speed;
    addition_right = addition_right + (new_addition_left - addition_left);
    addition_left = new_addition_left;
  }
  if (wrap_right_bw) {
    int8_t new_addition_right = INT8_MIN + speed;
    addition_left = addition_left + (new_addition_right - addition_right);
    addition_right = new_addition_right;
  }

  movement_output->left_motor.speed = speed + addition_left;
  movement_output->right_motor.speed = speed + addition_right;
  

  uart_putln_int(movement_output->left_motor.speed);
  uart_putln_int(movement_output->right_motor.speed);
  uart_putln("");
}

void movement_controller_setup(void) {
}

//Processes remote controller codes and convert them into something useful
//to send to a motor driver.
void movement_controller_loop(movement_data_t *data,
			      inp_decoder_output_t *inp_decoder_output,
			      movement_output_t *movement_output) {

  //only do stuff if we have actually received data.
  if (!inp_decoder_output->new_data) return;
  calc_motor_outputs(data, inp_decoder_output, movement_output);
}
