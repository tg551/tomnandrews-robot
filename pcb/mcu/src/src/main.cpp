#include <stdlib.h>
#include <stdint.h>

#include <avr/io.h>
#include <avr/interrupt.h>

//#define F_CPU 16000000
#include <util/delay.h>

#include "main.h"
#include "arduino_ports.h"
#include "uart.h"
#include "toms_uart_extras.h"
#include "timer1_setup.h"
#include "remote_rx.h"
#include "input_decoder.h"
#include "movement_controller.h"
//#include "motor_controller.h"


remote_rx_data_t rx_internal;
remote_rx_raw_output_t rx_output;

inp_decoder_data_t inp_decoder_internal;
inp_decoder_output_t inp_decoder_output;

movement_data_t movement_internal;
movement_output_t movement_output;

int main(void) {
  setup();
  while (1) loop();
}

void setup(void) {
  cli();
  uart_init(UART_BAUD_SELECT(57600, F_CPU));
  sei();

  timer1_setup();
  input_decoder_setup();
  movement_controller_setup();
  //motor_controller_setup_left();
  //motor_controller_setup_right();
}

void loop(void) {
  remote_rx_loop(&rx_internal, &rx_output);
  input_decoder_loop(&inp_decoder_internal, &rx_output, &inp_decoder_output);  
  movement_controller_loop(&movement_internal, &inp_decoder_output, &movement_output);
  //motor_controller_loop(&movement_data.left_motor, &movement_data.right_motor);
}
