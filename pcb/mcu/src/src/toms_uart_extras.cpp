#include <stdlib.h>

#include <avr/io.h>

#include "uart.h"
#include "toms_uart_extras.h"

extern void uart_putln(const char *s) {
  uart_puts(s);
  uart_puts("\r\n");
}

extern void uart_putln_int(int num){
  char chars [50];
  itoa((int)num, chars, 10);
  uart_putln(chars);
}

extern void uart_putln_uint32(uint32_t num){
  char chars [50];
  itoa((uint32_t)num, chars, 10);
  uart_putln(chars);
}
