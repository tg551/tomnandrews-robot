#ifndef REMOTE_RX_H
#define REMOTE_RX_H

#include <stdint.h>


/*
  struct to hold received raw data from the remote control.
  The typical range on my transmitter appears to be 17k-31k.
  NOTE: the data is this struct is only valid on the mainloop cycle that the 
  new_data flag is set.
*/
typedef struct remote_rx_raw_output_t {
  uint16_t channels[6];
  bool new_data_received;
} remote_rx_raw_output_t;

typedef struct remote_rx_internal_t {
  uint16_t next_channel;
  uint16_t last_pulse;
  uint8_t overflows;
} remote_rx_data_t;

void remote_rx_setup(void);
void remote_rx_loop(remote_rx_data_t*,
		    remote_rx_raw_output_t*);

#endif
